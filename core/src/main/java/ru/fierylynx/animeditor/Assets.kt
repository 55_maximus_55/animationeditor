package ru.fierylynx.animeditor

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.audio.Music
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.BitmapFont
import java.util.*

class Assets {
    val font = BitmapFont(Gdx.files.internal("fonts/Roboto_16.fnt"), Gdx.files.internal("fonts/Roboto_16.png"), false)
    private val textures = HashMap<String, Texture>()
    val sounds = HashMap<String, Sound>()
    val musics = HashMap<String, Music>()

    fun getTexture(name: String) : Texture? {
        if (textures.containsKey(name))
            return textures[name]
        if (Gdx.files.internal(name).exists()) {
            textures[name] = Texture(name)
            return textures[name]
        }
        return null
    }
}