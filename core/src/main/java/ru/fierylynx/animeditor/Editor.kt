package ru.fierylynx.animeditor

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.viewport.ScreenViewport
import ktx.actors.onChange
import ktx.actors.onClick
import ktx.app.clearScreen
import ktx.app.use
import de.tomgrill.gdxdialogs.core.GDXDialogsSystem
import ktx.app.KtxScreen
import java.util.*
import kotlin.collections.HashMap
import de.tomgrill.gdxdialogs.core.dialogs.GDXButtonDialog
import de.tomgrill.gdxdialogs.core.dialogs.GDXTextPrompt
import de.tomgrill.gdxdialogs.core.listener.TextPromptListener

class Editor(val name : String) : KtxScreen {

    val stage = Stage(ScreenViewport())
    var batch = SpriteBatch()
    var shapeRenderer = ShapeRenderer()
    var skin = Skin()
    var textureAtlas = TextureAtlas(Gdx.files.absolute("$name.pack"))

    var width = 16
    var height = 32

    var selectedAnim = ""
    var selectedBone = ""
    var selectedTime = 0f

    var animations = HashMap<String, Animation>()
    var bones = HashMap<String, Bone>()
    var bonesNames = ArrayList<String>()

    var dialogs = GDXDialogsSystem.install()

    var timer = 0f
    var play = false
    var loop = false

    override fun show() {
        skin.addRegions(textureAtlas)
        initEditor()
        Gdx.input.inputProcessor = stage
        createStage()
        stage.setDebugAll(true)
    }

    fun createStage() {
        stage.clear()
        stage.addActor(Table().apply {
            top()
            left()
            setFillParent(true)

            add(Label("anims", Main.styles.labelStyle))
            row()
            add(SelectBox<String>(Main.styles.selectBoxStyle).apply {
                val a = Array<String>()
                for (i in animations.keys)
                    a.add(i)
                items = a
                selected = selectedAnim
                onChange {
                    selectedAnim = selected
                    selectedTime = 0f
                    createStage()
                }
            })
            add(TextButton("-", Main.styles.textButtonStyle).apply {
                addListener(onClick {
                    removeAnim()
                })
            })
            add(TextButton("+", Main.styles.textButtonStyle).apply {
                addListener(onClick {
                    addAnim()
                })
            })
        })
        stage.addActor(Table().apply {
            top()
            right()
            setFillParent(true)

            add(Label("bones", Main.styles.labelStyle))
            row()
            add(SelectBox<String>(Main.styles.selectBoxStyle).apply {
                val a = Array<String>()
                for (i in bonesNames)
                    a.add(i)
                items = a
                selected = selectedBone
                onChange {
                    selectedBone = selected
                    createStage()
                }
            })
            add(TextButton("-", Main.styles.textButtonStyle).apply {
                addListener(onClick {
                    if (bonesNames.indexOf(selectedBone) > 0) {
                        val a = bonesNames.indexOf(selectedBone)
                        val b = bonesNames.indexOf(selectedBone) - 1
                        bonesNames[a] = bonesNames[b]
                        bonesNames[b] = selectedBone
                        createStage()
                    }
                })
            })
            add(TextButton("+", Main.styles.textButtonStyle).apply {
                addListener(onClick {
                    if (bonesNames.indexOf(selectedBone) < bonesNames.size - 1) {
                        val a = bonesNames.indexOf(selectedBone)
                        val b = bonesNames.indexOf(selectedBone) + 1
                        bonesNames[a] = bonesNames[b]
                        bonesNames[b] = selectedBone
                        createStage()
                    }
                })
            })
        })
        stage.addActor(Table().apply {
            top()
            setFillParent(true)

            add(Label("times", Main.styles.labelStyle))
            row()
            add(SelectBox<String>(Main.styles.selectBoxStyle).apply {
                val a = Array<String>()
                for (i in animations[selectedAnim]!!.times)
                    a.add(i.toString())
                items = a
                selected = selectedTime.toString()
                onChange {
                    selectedTime = selected.toFloat()
                    createStage()
                }
            })
            add(TextButton("-", Main.styles.textButtonStyle).apply {
                addListener(onClick {
                    removeTime()
                })
            })
            add(TextButton("+", Main.styles.textButtonStyle).apply {
                addListener(onClick {
                    addTime()
                })
            })
        })
    }

    override fun render(delta: Float) {
        clearScreen(1f, 1f, 1f, 1f)

        val h = (Gdx.graphics.height - 72f - 60 - 10) / height.toFloat()
        val w = (Gdx.graphics.width - 20f) / width.toFloat()
        val s = Math.min(h, w)

        val x0 = (Gdx.graphics.width / 2 - width * s / 2)
        val y0 = (60f + (Gdx.graphics.height - 72f - 60 - 10) / 2 - height * s / 2)

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled)
        shapeRenderer.color = Color.BLACK
        shapeRenderer.rect(10f, 10f, 10f, 40f)
        shapeRenderer.rect(20f, 25f, Gdx.graphics.width - 40f, 10f)
        shapeRenderer.rect(Gdx.graphics.width - 20f, 10f, 10f, 40f)
        for (i in animations[selectedAnim]!!.times) {
            if (i == selectedTime)
                shapeRenderer.color = Color.GREEN
            else
                shapeRenderer.color = Color.RED
            shapeRenderer.circle(15f + (Gdx.graphics.width - 15f - 15f) * (if (i == 0f) 0f else (i / animations[selectedAnim]!!.times.last())), 30f, 7f)
        }
        shapeRenderer.color = Color.BLUE
        if (play)
            shapeRenderer.circle(15f + (Gdx.graphics.width - 15f - 15f) * (if (timer == 0f) 0f else (timer / animations[selectedAnim]!!.times.last())), 30f, 7f)
        shapeRenderer.end()

        if (play) {
            timer += delta
            if (timer > animations[selectedAnim]!!.times.last()) {
                if (loop)
                    timer -= animations[selectedAnim]!!.times.last()
                else
                    play = false
            }
        }

        if (play) {
            var index = 0
            for (i in animations[selectedAnim]!!.times) {
                if (animations[selectedAnim]!!.times[index] < timer)
                    index++
            }
            batch.use {
                val a = animations[selectedAnim]!!.times[index - 1]
                val b = animations[selectedAnim]!!.times[index]
                for (bone in bonesNames) {
                    val a1 = animations[selectedAnim]!!.states[a]!![bone]!!.angle
                    val a2 = animations[selectedAnim]!!.states[b]!![bone]!!.angle
                    val b1 = (a2 - a1)
                    var b2: Float
                    if (Math.abs((a2 - a1) - 360) < Math.abs(360 - (a1 - a2)))
                        b2 = (a2 - a1) - 360
                    else
                        b2 = 360 - (a1 - a2)

                    var angle: Float
                    if (Math.abs(b1) < Math.abs(b2)) {
                        angle = b1 * (timer - a) / (b - a)
                    } else {
                        angle = b2 * (timer - a) / (b - a)
                    }
                    batch.draw(bones[bone]!!.textureRegion,
                            x0 + animations[selectedAnim]!!.states[a]!![bone]!!.x * s + (animations[selectedAnim]!!.states[b]!![bone]!!.x * s - animations[selectedAnim]!!.states[a]!![bone]!!.x * s) * (timer - a) / (b - a),
                            y0 + animations[selectedAnim]!!.states[a]!![bone]!!.y * s + (animations[selectedAnim]!!.states[b]!![bone]!!.y * s - animations[selectedAnim]!!.states[a]!![bone]!!.y * s) * (timer - a) / (b - a),
                            bones[bone]!!.width * s / 2,
                            bones[bone]!!.height * s / 2,
                            bones[bone]!!.width * s,
                            bones[bone]!!.height * s,
                            1f, 1f,
                            animations[selectedAnim]!!.states[a]!![bone]!!.angle + angle)
                }
            }
        } else {
            batch.use {
                for (bone in bonesNames) {
                    batch.draw(bones[bone]!!.textureRegion,
                            x0 + animations[selectedAnim]!!.states[selectedTime]!![bone]!!.x * s,
                            y0 + animations[selectedAnim]!!.states[selectedTime]!![bone]!!.y * s,
                            bones[bone]!!.width * s / 2,
                            bones[bone]!!.height * s / 2,
                            bones[bone]!!.width * s,
                            bones[bone]!!.height * s,
                            1f, 1f,
                            animations[selectedAnim]!!.states[selectedTime]!![bone]!!.angle)
                }
            }
        }

        shapeRenderer.begin(ShapeRenderer.ShapeType.Line)
        shapeRenderer.color = Color.RED
        shapeRenderer.rect(10f, 60f, Gdx.graphics.width - 20f, Gdx.graphics.height - 72f - 60 - 10)
        shapeRenderer.color = Color.BLACK

        shapeRenderer.rect(Gdx.graphics.width / 2 - width * s / 2, 60f + (Gdx.graphics.height - 72f - 60 - 10) / 2 - height * s / 2, width * s, height * s)
        shapeRenderer.end()

        stage.act(delta)
        stage.draw()
        if (Gdx.input.isKeyPressed(Input.Keys.E)) {
            animations[selectedAnim]!!.states[selectedTime]!![selectedBone]!!.x = (Gdx.input.x - (Gdx.graphics.width / 2 - width * s / 2)) / s - bones[selectedBone]!!.width / 2
            animations[selectedAnim]!!.states[selectedTime]!![selectedBone]!!.y = (Gdx.graphics.height - Gdx.input.y - (60f + (Gdx.graphics.height - 72f - 60 - 10) / 2 - height * s / 2)) / s - bones[selectedBone]!!.height / 2
        }
        if (Gdx.input.isKeyPressed(Input.Keys.R)) {
            animations[selectedAnim]!!.states[selectedTime]!![selectedBone]!!.angle =
                    Vector2((Gdx.input.x - (Gdx.graphics.width / 2 - width * s / 2)) / s - bones[selectedBone]!!.width / 2, (Gdx.graphics.height - Gdx.input.y - (60f + (Gdx.graphics.height - 72f - 60 - 10) / 2 - height * s / 2)) / s - bones[selectedBone]!!.height / 2).sub(animations[selectedAnim]!!.states[selectedTime]!![selectedBone]!!.x, animations[selectedAnim]!!.states[selectedTime]!![selectedBone]!!.y).angle()
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.P)) {
            if (play)
                play = false
            else {
                play = true
                timer = 0f
            }
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.L)) {
            loop = !loop
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.F1)) {
            val bDialog = dialogs.newDialog(GDXButtonDialog::class.java)
            bDialog.setTitle("HELP")
            bDialog.setMessage("E - move selected bone\n" +
                    "R - rotate selected bone\n" +
                    "P - play selected animation\n" +
                    "L - loop play animation\n" +
                    "A - rename selected animation\n" +
                    "T - edit selected time")

            bDialog.setClickListener {

            }
            bDialog.addButton("Ok")
            bDialog.build().show()
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.F2)) {
            val textPrompt = dialogs.newDialog(GDXTextPrompt::class.java)

            textPrompt.setTitle("Width")
            textPrompt.setCancelButtonLabel("Close")
            textPrompt.setConfirmButtonLabel("Ok")

            textPrompt.setTextPromptListener(object : TextPromptListener {
                override fun confirm(text: String) {
                    width = text.toInt()
                    val textPrompt1 = dialogs.newDialog(GDXTextPrompt::class.java)

                    textPrompt1.setTitle("Height")

                    textPrompt1.setCancelButtonLabel("Close")
                    textPrompt1.setConfirmButtonLabel("Ok")

                    textPrompt1.setTextPromptListener(object : TextPromptListener {
                        override fun confirm(text: String) {
                            height = text.toInt()
                        }

                        override fun cancel() {

                        }
                    })
                    textPrompt1.build().show()
                }

                override fun cancel() {

                }
            })
            textPrompt.build().show()
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.A)) {
            val textPrompt = dialogs.newDialog(GDXTextPrompt::class.java)

            textPrompt.setTitle("Rename animation")
            textPrompt.setMessage("animation name")

            textPrompt.setCancelButtonLabel("Close")
            textPrompt.setConfirmButtonLabel("Rename")

            textPrompt.setTextPromptListener(object : TextPromptListener {
                override fun confirm(text: String) {
                    renameAnim(selectedAnim, text)
                }

                override fun cancel() {

                }
            })
            textPrompt.build().show()
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.T)) {
            val textPrompt = dialogs.newDialog(GDXTextPrompt::class.java)

            textPrompt.setTitle("Edit time")
            textPrompt.setMessage("new time")

            textPrompt.setCancelButtonLabel("Close")
            textPrompt.setConfirmButtonLabel("Edit")

            textPrompt.setTextPromptListener(object : TextPromptListener {
                override fun confirm(text: String) {
                    renameTime(selectedTime, text.toFloat())
                }

                override fun cancel() {

                }
            })
            textPrompt.build().show()
        }
    }

    override fun resize(width: Int, height: Int) {
        shapeRenderer = ShapeRenderer()
        batch = SpriteBatch()
        stage.viewport.update(width, height, true)
    }

    class Animation {
        val times = ArrayList<Float>()
        val states = HashMap<Float, HashMap<String, BoneAnim>>()
    }

    class Bone {
        lateinit var textureRegion : TextureRegion
        var width = 1f
        var height = 1f
    }

    class BoneAnim {
        var x = 0f
        var y = 0f
        var angle = 0f
    }

    fun initEditor() {
        if (Gdx.files.absolute("$name.anim").exists())
            load()
        else {
            selectedAnim = "anim"
            animations[selectedAnim] = Animation()

            selectedTime = 0f
            animations[selectedAnim]!!.times.add(selectedTime)

            animations[selectedAnim]!!.states[selectedTime] = HashMap()
            for (i in textureAtlas.regions) {
                addBone(i.name)
            }
            selectedBone = bonesNames.first()
        }
    }

    fun addAnim() {
        selectedTime = 0f
        val t = selectedAnim
        selectedAnim = "anim"
        while (animations.containsKey(selectedAnim))
            selectedAnim += '0'
        animations[selectedAnim] = Animation()
        animations[selectedAnim]!!.times.add(selectedTime)

        animations[selectedAnim]!!.states[selectedTime] = HashMap()
        for (i in bonesNames) {
            animations[selectedAnim]!!.states[selectedTime]!![i] = BoneAnim()
            animations[selectedAnim]!!.states[selectedTime]!![i]!!.x = animations[t]!!.states[selectedTime]!![i]!!.x
            animations[selectedAnim]!!.states[selectedTime]!![i]!!.y = animations[t]!!.states[selectedTime]!![i]!!.y
            animations[selectedAnim]!!.states[selectedTime]!![i]!!.angle = animations[t]!!.states[selectedTime]!![i]!!.angle
        }
        createStage()
    }

    fun addBone(text: String) {
        selectedBone = text
        while (bonesNames.contains(selectedBone))
            selectedBone += '0'
        bonesNames.add(selectedBone)
        bones[selectedBone] = Bone()
        for (anim in animations.values) {
            for (time in anim.times) {
                anim.states[time]!![selectedBone] = BoneAnim()
            }
        }
        bones[selectedBone]!!.textureRegion = skin.getRegion(selectedBone)
        bones[selectedBone]!!.width = bones[selectedBone]!!.textureRegion.regionWidth.toFloat()
        bones[selectedBone]!!.height = bones[selectedBone]!!.textureRegion.regionHeight.toFloat()
        createStage()

    }

    fun addTime() {
        if (animations[selectedAnim]!!.times.last() == selectedTime) {
            val t = selectedTime
            selectedTime += 1f
            animations[selectedAnim]!!.times.add(selectedTime)
            animations[selectedAnim]!!.states[selectedTime] = HashMap()
            for (i in bonesNames) {
                animations[selectedAnim]!!.states[selectedTime]!![i] = BoneAnim()
                animations[selectedAnim]!!.states[selectedTime]!![i]!!.x = animations[selectedAnim]!!.states[t]!![i]!!.x
                animations[selectedAnim]!!.states[selectedTime]!![i]!!.y = animations[selectedAnim]!!.states[t]!![i]!!.y
                animations[selectedAnim]!!.states[selectedTime]!![i]!!.angle = animations[selectedAnim]!!.states[t]!![i]!!.angle
            }
        }
        else {
            val t = selectedTime
            selectedTime = (selectedTime + animations[selectedAnim]!!.times[animations[selectedAnim]!!.times.indexOf(selectedTime) + 1]) / 2
            animations[selectedAnim]!!.times.add(selectedTime)
            animations[selectedAnim]!!.states[selectedTime] = HashMap()
            for (i in bonesNames) {
                animations[selectedAnim]!!.states[selectedTime]!![i] = BoneAnim()
                animations[selectedAnim]!!.states[selectedTime]!![i]!!.x = animations[selectedAnim]!!.states[t]!![i]!!.x
                animations[selectedAnim]!!.states[selectedTime]!![i]!!.y = animations[selectedAnim]!!.states[t]!![i]!!.y
                animations[selectedAnim]!!.states[selectedTime]!![i]!!.angle = animations[selectedAnim]!!.states[t]!![i]!!.angle
            }
        }
        animations[selectedAnim]!!.times.sort()
        createStage()
    }

    fun removeAnim() {
        if (animations.size > 1) {
            animations.remove(selectedAnim)
            selectedAnim = animations.keys.first()
            selectedTime = 0f
            createStage()
        }
    }

    fun removeTime() {
        if (animations[selectedAnim]!!.times.size > 1) {
            if (selectedTime == 0f) {
                animations[selectedAnim]!!.states.remove(selectedTime)
                animations[selectedAnim]!!.times.remove(selectedTime)
                renameTime(animations[selectedAnim]!!.times.first(), 0f)
            }
            else {
                animations[selectedAnim]!!.states.remove(selectedTime)
                animations[selectedAnim]!!.times.remove(selectedTime)
                selectedTime = 0f
                createStage()
            }
        }
    }

    fun renameAnim(a: String, b: String) {
        if (!animations.containsKey(b)) {
            animations[b] = animations[a]!!
            animations.remove(a)
            selectedAnim = b
            createStage()
        }
    }

    fun renameTime(a: Float, b: Float) {
        if (!animations[selectedAnim]!!.times.contains(b)) {
            animations[selectedAnim]!!.states[b] = animations[selectedAnim]!!.states[a]!!
            animations[selectedAnim]!!.states.remove(a)

            animations[selectedAnim]!!.times.remove(a)
            animations[selectedAnim]!!.times.add(b)
            animations[selectedAnim]!!.times.sort()
            selectedTime = b
            createStage()
        }
    }

    fun save() {
        var file = Gdx.files.absolute("$name.anim")
        file.writeString("bones {\n\t", false)
        for (bone in bonesNames)
            file.writeString("$bone ", true)
        file.writeString("\n}\n", true)
        for (anim in animations.keys) {
            file.writeString("$anim {", true)
            for (time in animations[anim]!!.times) {
                file.writeString("\n\t$time {", true)
                for (bone in bonesNames) {
                    file.writeString("\n\t\t$bone", true)
                    file.writeString(" " + animations[anim]!!.states[time]!![bone]!!.x, true)
                    file.writeString(" " + animations[anim]!!.states[time]!![bone]!!.y, true)
                    file.writeString(" " + animations[anim]!!.states[time]!![bone]!!.angle, true)
                }
                file.writeString("\n\t}", true)
            }
            file.writeString("\n}\n", true)
        }
    }

    fun load() {
        var file = Gdx.files.absolute("$name.anim")
        val input = Scanner(file.read())
        while (input.hasNext()) {
            val x = input.next()
            if (x == "bones") {
                input.next()
                var bon = input.next()
                while (bon != "}") {
                    bonesNames.add(bon)
                    bones[bon] = Bone()
                    bones[bon]!!.textureRegion = skin.getRegion(bon)
                    bones[bon]!!.width = bones[bon]!!.textureRegion.regionWidth.toFloat()
                    bones[bon]!!.height = bones[bon]!!.textureRegion.regionHeight.toFloat()
                    bon = input.next()
                }
            }
            else {
                animations[x] = Animation()
                input.next()
                var tm = input.next()
                while (tm != "}") {
                    input.next()
                    animations[x]!!.times.add(tm.toFloat())
                    animations[x]!!.states[tm.toFloat()] = HashMap()
                    var bn = input.next()
                    while (bn != "}") {
                        animations[x]!!.states[tm.toFloat()]!![bn] = BoneAnim()
                        animations[x]!!.states[tm.toFloat()]!![bn]!!.x = input.next().toFloat()
                        animations[x]!!.states[tm.toFloat()]!![bn]!!.y = input.next().toFloat()
                        animations[x]!!.states[tm.toFloat()]!![bn]!!.angle = input.next().toFloat()
                        bn = input.next()
                    }
                    tm = input.next()
                }
            }
        }
        selectedBone = bonesNames.first()
        selectedAnim = animations.keys.first()
        selectedTime = 0f
    }

    override fun hide() {
        save()
    }
}
