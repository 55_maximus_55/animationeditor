package ru.fierylynx.animeditor

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.ui.List
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable
import ktx.app.color

class Styles {
    val labelStyle = Label.LabelStyle().apply {
        font = Main.assets.font
        fontColor = Color.WHITE
    }

    val textButtonStyle = TextButton.TextButtonStyle().apply {
        font = Main.assets.font
        fontColor = Color.WHITE
    }

    val listSStyle = List.ListStyle().apply {
        font = Main.assets.font
        fontColorSelected = Color.RED
        fontColorUnselected = Color.BLACK
        selection = SpriteDrawable(Sprite(Main.assets.getTexture("a.png")))
    }
    val scrollPaneStyle = ScrollPane.ScrollPaneStyle().apply {

    }

    val selectBoxStyle = SelectBox.SelectBoxStyle().apply {
        font = Main.assets.font
        fontColor = Color.BLACK
        scrollStyle = scrollPaneStyle
        listStyle = listSStyle
    }

    val fileManagerStyle = FileManagerStyle()
    class FileManagerStyle {
        val pathLabelStyle = Label.LabelStyle().apply {
            font = Main.assets.font
            fontColor = Color.WHITE
        }

        val folderTextButtonStyle = TextButton.TextButtonStyle().apply {
            font = Main.assets.font
            fontColor = Color.GREEN
        }
        val pngTextButtonStyle = TextButton.TextButtonStyle().apply {
            font = Main.assets.font
            fontColor = Color.RED
        }
        val animTextButtonStyle = TextButton.TextButtonStyle().apply {
            font = Main.assets.font
            fontColor = Color.WHITE
        }
    }

    val editorStyle = EditorStyle()
    class EditorStyle {
        val labelStyle = Label.LabelStyle().apply {
            font = Main.assets.font
            fontColor = Color.WHITE
        }

        val textButtonStyle = TextButton.TextButtonStyle().apply {
            font = Main.assets.font
            fontColor = Color.GREEN
        }
    }
}