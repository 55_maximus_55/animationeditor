package ru.fierylynx.animeditor

import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Screen
import de.tomgrill.gdxdialogs.core.GDXDialogsSystem
import de.tomgrill.gdxdialogs.core.listener.TextPromptListener
import de.tomgrill.gdxdialogs.core.dialogs.GDXTextPrompt
import java.io.File

class Main : Game() {

    companion object {
        private lateinit var instance: Main

        lateinit var assets: Assets
        lateinit var styles: Styles

        fun setScreen(screen: Screen) {
            instance.setScreen(screen)
        }
    }

    override fun create() {
        instance = this

        assets = Assets()
        styles = Styles()


        setScreen(FileManagerScreen())
    }
}