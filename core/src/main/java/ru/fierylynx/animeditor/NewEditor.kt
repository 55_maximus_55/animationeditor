package ru.fierylynx.animeditor

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.utils.viewport.ScreenViewport
import ktx.app.KtxScreen
import ktx.app.clearScreen

class NewEditor(path: String) : KtxScreen {

    val stage = Stage(ScreenViewport())
    val batch = SpriteBatch()
    val shapeRenderer = ShapeRenderer()

    var skin = Skin()
    var textureAtlas = TextureAtlas(Gdx.files.absolute("$path.pack"))

    override fun show() {
        Gdx.input.inputProcessor = stage
        recreateTable()
    }

    override fun render(delta: Float) {
        clearScreen(0f, 0f, 0f)

        stage.act()
        stage.draw()
    }

    override fun resize(width: Int, height: Int) {
        stage.viewport.update(width, height, true)
    }

    fun recreateTable() {
        stage.apply {
            val tableTop = Table().apply {
                val exitButton = TextButton("Exit", Main.styles.editorStyle.textButtonStyle)
                val saveButton = TextButton("Save", Main.styles.editorStyle.textButtonStyle)

                setFillParent(true)
                top()
                left()
                add(exitButton).padRight(8f)
                add(saveButton)
            }
            val tableLeft = Table().apply {
                val animLabel = Label("Animations", Main.styles.editorStyle.labelStyle)

                setFillParent(true)
                top()
                left()
                add(animLabel).padTop(24f).row()
                add(ScrollPane(Table().apply {
                    
                })).padBottom(64f)
            }
            val tableLeftBottom = Table().apply {
                val animSettingsLabel = Label("Anim options", Main.styles.editorStyle.labelStyle)
                val t = Table().apply {
                    val nameLabel = Label("Name", Main.styles.editorStyle.labelStyle)
                    val timeLabel = Label("Time", Main.styles.editorStyle.labelStyle)

                    add(nameLabel).row()
                    add(timeLabel)
                }

                setFillParent(true)
                bottom()
                left()
                add(animSettingsLabel).row()
                add(t)
            }
            val tableRight = Table().apply {
                val animLabel = Label("Animations", Main.styles.editorStyle.labelStyle)

                setFillParent(true)
                top()
                right()
                add(animLabel).padTop(24f).row()
                add(ScrollPane(Table().apply {

                })).padBottom(64f)
            }
            val tableRightBottom = Table().apply {
                val animSettingsLabel = Label("Bone options", Main.styles.editorStyle.labelStyle)
                val t = Table().apply {
                    val xLabel = Label("X", Main.styles.editorStyle.labelStyle)
                    val yLabel = Label("Y", Main.styles.editorStyle.labelStyle)
                    val angleLabel = Label("angle", Main.styles.editorStyle.labelStyle)

                    add(xLabel).row()
                    add(yLabel).row()
                    add(angleLabel)
                }

                setFillParent(true)
                bottom()
                right()
                add(animSettingsLabel).row()
                add(t)
            }


            clear()
            addActor(tableTop)
            addActor(tableLeft)
            addActor(tableLeftBottom)
            addActor(tableRight)
            addActor(tableRightBottom)
        }
    }

}