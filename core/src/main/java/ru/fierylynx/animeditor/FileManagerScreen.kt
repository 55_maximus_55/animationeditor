package ru.fierylynx.animeditor

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.utils.viewport.ScreenViewport
import ktx.actors.onClick
import ktx.app.KtxScreen
import ktx.app.clearScreen

class FileManagerScreen : KtxScreen {

    var table = Table()
    var pathLabel = Label("kek", Main.styles.fileManagerStyle.pathLabelStyle)
    var stage = Stage(ScreenViewport()).apply {
        addActor(Table().apply {
            setFillParent(true)
            top()
            left()

            add(pathLabel).row()
            add(ScrollPane(table))
        })
    }

    var path = Gdx.files.localStoragePath

    override fun show() {
        Gdx.input.inputProcessor = stage
        recreateTable()
    }

    override fun render(delta: Float) {
        clearScreen(0f, 0f, 0f)
        stage.act()
        stage.draw()
    }

    fun recreateTable() {
        pathLabel.setText(path)
        table.apply {
            clear()
            left()
            top()
            add(TextButton("..", Main.styles.fileManagerStyle.folderTextButtonStyle).apply {
                onClick {
                    path = Gdx.files.absolute(path).parent().path()
                    recreateTable()
                }
            })

            for (i in Gdx.files.absolute(path).list()) {
                row()
                if (i.isDirectory) {
                    val textButton = TextButton(i.name(), Main.styles.fileManagerStyle.folderTextButtonStyle).apply {
                        onClick {
                            path = i.path()
                            recreateTable()
                        }
                    }
                    add(textButton)
                }
                else if (i.extension() == "png") {
                    if (Gdx.files.absolute(i.pathWithoutExtension() + ".pack").exists()) {
                        val textButton = TextButton(i.name(), Main.styles.fileManagerStyle.animTextButtonStyle).apply {
                            onClick {
                                Gdx.input.inputProcessor = null
                                Main.setScreen(NewEditor(i.pathWithoutExtension()))
                            }
                        }
                        add(textButton)
                    } else {
                        val textButton = TextButton(i.name(), Main.styles.fileManagerStyle.pngTextButtonStyle).apply {
                            onClick {

                            }
                        }
                        add(textButton)
                    }
                }
            }
        }
    }

    override fun resize(width: Int, height: Int) {
        stage.viewport.update(width, height, true)
    }
}